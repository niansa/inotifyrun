all:
	gcc -rdynamic -O3 inotifyrun.c libcatch/libcatch.c -o inotifyrun

debug:
	gcc -rdynamic -O0 -g inotifyrun.c libcatch/libcatch.c -o inotifyrun

install:
	cp inotifyrun /usr/local/bin/
