# inotifyrun
Run a command every time a file is opened

## Setup
1. Clone: `git clone https://gitlab.com/niansa/inotifyrun.git --recursive && cd inotifyrun`
2. Compile: `make`
3. Install: `sudo install`

## Usage
This should be self-explanatory:

`inotifyrun ./test.sh "echo hi" ~/Documents/table.odt "killall libreoffice"`
